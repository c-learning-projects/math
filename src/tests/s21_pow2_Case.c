#include "tests.h"

START_TEST(normalEqualTest) {
    double test_arr[][2] = {{2, 2}, {0, 2}, {2, 0},
                            {18, 6}, {-4, 2}};

    int arrMax = 5;
    for (int i = 0; i < arrMax; i++) {
        long double origResult = pow(test_arr[i][0], test_arr[i][1]);
        long double s21Result = s21_pow2(test_arr[i][0], test_arr[i][1]);
        ck_assert_msg(d_comp(origResult, s21Result),
                          "\nfail nrm:  org pow(%.2lf^%.2lf) = %.8Lf  | "
                          "s21_pow2(%.2lf^%.2lf) = %.8Lf \n",
                          test_arr[i][0], test_arr[i][1], origResult,
                          test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

START_TEST(lessEqualTest) {
    double test_arr[][2] = {{-0.2, 2}, {-0.0, 2}, {-0.4, 2}};
    int arrMax = 3;
    for (int i = 0; i < arrMax; i++) {
        long double origResult = pow(test_arr[i][0], test_arr[i][1]);
        long double s21Result = s21_pow2(test_arr[i][0], test_arr[i][1]);
        ck_assert_msg(d_comp(origResult, s21Result),
                          "\nfail less:  org pow(%.2lf^%.2lf) = %.8Lf | "
                          "s21_pow2(%.2lf^%.2lf) = %.8Lf\n",
                          test_arr[i][0], test_arr[i][1], origResult,
                          test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

START_TEST(moreEqualTest) {
    double test_arr[][2] = {{20, 2}, {1110, 2}, {2, 0}, {185, 16}, {-4, 19}};

    int arrMax = 5;
    long double origResult;
    long double s21Result;
    for (int i = 0; i < arrMax; i++) {
        origResult = pow(test_arr[i][0], test_arr[i][1]);
        s21Result = s21_pow2(test_arr[i][0], test_arr[i][1]);
        ck_assert_msg(d_comp(origResult, s21Result),
                      "\nfail more:  org pow(%.2lf^%.2lf) = %.8Lf | "
                      "s21_pow2(%.2lf^%.2lf) = %.8Lf\n",
                      test_arr[i][0], test_arr[i][1], origResult,
                      test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

START_TEST(charEqualTest) {
    char test_arr[][2] = {{'3', 2}, {'Q', 2}, {'\n', 0}};
    int arrMax = 3;
    for (int i = 0; i < arrMax; i++) {
        long double origResult = pow(test_arr[i][0], test_arr[i][1]);
        long double s21Result = s21_pow2(test_arr[i][0], test_arr[i][1]);
        ck_assert_msg(d_comp(origResult, s21Result),
                          "\nchr fail: org pow(%.2d^%.2d) = %.8Lf | "
                          "s21_pow2(%.2d^%.2d) = %.8Lf\n",
                          test_arr[i][0], test_arr[i][1], origResult,
                          test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

TCase* Creates_21_pow2_Case() {
    TCase* s21_pow2_Case = tcase_create("s21_pow2_Case");

    tcase_add_test(s21_pow2_Case, normalEqualTest);
    tcase_add_test(s21_pow2_Case, lessEqualTest);
    tcase_add_test(s21_pow2_Case, moreEqualTest);
    tcase_add_test(s21_pow2_Case, charEqualTest);

    return s21_pow2_Case;
}
