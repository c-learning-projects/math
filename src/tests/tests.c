#include "tests.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int d_sub_comp(char* f, char* s) {
    int res = 1, i = 0, j = 0;
    while ((i < 16) && (f[i] == s[i]) && (f[i] != '.') && (s[i] != '.')) i++;
    if (i == 16) {
        i = ((int)(strchr(f, '.') - f)) + 1;
    }
    while ((j < 6) && (f[i + j] == s[i + j])) j++;
    if (j < 6) res = 0;
    return res;
}

int d_comp(long double first, long double second) {
    int res = 0;
    if (first == second) res = 1;
    if ((res == 0) && (first < second + S21_ACCURITY) && (first > second - S21_ACCURITY)) res = 1;
    if ((res == 0) && (isnan(first) && isnan(second))) res = 1;
    if ((res == 0) && (isinf(first) && isinf(second))) res = 1;
    if (res == 0) {
        char first_ch[100];
        snprintf(first_ch, sizeof(first_ch), "%16.10Lf", first);
        char second_ch[100];
        snprintf(second_ch, sizeof(second_ch), "%16.10Lf", second);
        if (d_sub_comp(first_ch, second_ch)) res = 1;
    }
    return res;
}

int main(void) {
    Suite *s1 = suite_create("Core");
    SRunner *sr = srunner_create(s1);

    TCase *s21_sqrt_Case = Creates_21_sqrt_Case();
    suite_add_tcase(s1, s21_sqrt_Case);

    TCase *s21_abs_Case = Creates_21_abs_Case();
    suite_add_tcase(s1, s21_abs_Case);

    TCase *s21_fabs_Case = Creates_21_fabs_Case();
    suite_add_tcase(s1, s21_fabs_Case);

    TCase *s21_floor_Case = Creates_21_floor_Case();
    suite_add_tcase(s1, s21_floor_Case);

    TCase *s21_cos_Case = Creates_21_cos_Case();
    suite_add_tcase(s1, s21_cos_Case);

    TCase *s21_sin_Case = Creates_21_sin_Case();
    suite_add_tcase(s1, s21_sin_Case);

    TCase *s21_pow_Case = Creates_21_pow_Case();
    suite_add_tcase(s1, s21_pow_Case);

    TCase *s21_fmod_Case = Creates_21_fmod_Case();
    suite_add_tcase(s1, s21_fmod_Case);

    TCase *s21_log_Case = Creates_21_log_Case();
    suite_add_tcase(s1, s21_log_Case);

    TCase *s21_exp_Case = Creates_21_exp_Case();
    suite_add_tcase(s1, s21_exp_Case);

    TCase *s21_ceil_Case = Creates_21_ceil_Case();
    suite_add_tcase(s1, s21_ceil_Case);

    TCase *s21_tan_Case = Creates_21_tan_Case();
    suite_add_tcase(s1, s21_tan_Case);

    TCase *s21_atan_Case = Creates_21_atan_Case();
    suite_add_tcase(s1, s21_atan_Case);

    TCase *s21_asin_Case = Creates_21_asin_Case();
    suite_add_tcase(s1, s21_asin_Case);

    TCase *s21_acos_Case = Creates_21_acos_Case();
    suite_add_tcase(s1, s21_acos_Case);

    TCase *s21_pow2_Case = Creates_21_pow2_Case();
    suite_add_tcase(s1, s21_pow2_Case);

    srunner_run_all(sr, CK_ENV);
    srunner_free(sr);
    return 0;
}
