#ifndef SRC_TESTS_TESTS_H_
#define SRC_TESTS_TESTS_H_
#include <check.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../s21_math.h"
int d_comp(long double first, long double second);
TCase* Creates_21_sqrt_Case();
TCase* Creates_21_abs_Case();
TCase* Creates_21_fabs_Case();
TCase* Creates_21_floor_Case();
TCase* Creates_21_cos_Case();
TCase* Creates_21_sin_Case();
TCase* Creates_21_pow_Case();
TCase* Creates_21_fmod_Case();
TCase* Creates_21_log_Case();
TCase* Creates_21_exp_Case();
TCase* Creates_21_ceil_Case();
TCase* Creates_21_tan_Case();
TCase* Creates_21_atan_Case();
TCase* Creates_21_asin_Case();
TCase* Creates_21_acos_Case();
TCase* Creates_21_pow2_Case();
#endif  // SRC_TESTS_TESTS_H_
