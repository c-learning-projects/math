#include "tests.h"

START_TEST(normalEqualTest) {
    double test_arr[] = {-4.9, -1.6, 14.4, 1.0, 3.0,  3.1, 3.2,
                         3.3,  -4.5, 5.0,  5.5, -6.0, 2.,  -.211};

    double arrMax = (double)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        double origResult = fabs(test_arr[i]);
        double s21Result = s21_fabs(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chk norm: orig fabs(%.2d) = %.8d | s21_fabs(%.2d) = %.8d\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "fail norm: orig fabs(%.2d) = %.8d | s21_fabs(%.2d) = %.8d\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(lessEqualTest) {
    double test_arr[] = {-.4000,   -1.6000, -1440000, -100000,
                         -300.000, -3.1111, -111.32};

    double arrMax = (double)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        double origResult = fabs(test_arr[i]);
        double s21Result = s21_fabs(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chk less: orig fabs(%.2d) = %.8d | s21_fabs(%.2d) = %.8d\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "fail less: orig fabs(%.2d) = %.8d | s21_fabs(%.2d) = %.8d\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(moreEqualTest) {
    double test_arr[] = {+111.1111, -1.6000, -1440.000, +100.6000,
                         +30000.0,  -31.111, -111.32};

    double arrMax = (double)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        double origResult = fabs(test_arr[i]);
        double s21Result = s21_fabs(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chk more: orig fabs(%.2d) = %.8d | s21_fabs(%.2d) = %.8d\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "fail more: orig fabs(%.2d) = %.8d | s21_fabs(%.2d) = %.8d\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(charEqualTest) {
    double test_arr[] = {'3', '7', 't', 'Q', '\n', '\t'};
    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        long double origResult = fabs(test_arr[i]);
        long double s21Result = s21_fabs(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chk chr: sqrt(%.2d) = %.8Lf | s21_sqrt(%.2d) = %.8Lf\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "chr fail: org fabs(%.2d) = %.8Lf | s21_fabs(%.2d) = %.8Lf\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

TCase* Creates_21_fabs_Case() {
    TCase* s21_fabs_Case = tcase_create("s21_fabs_Case");

    tcase_add_test(s21_fabs_Case, normalEqualTest);
    tcase_add_test(s21_fabs_Case, lessEqualTest);
    tcase_add_test(s21_fabs_Case, moreEqualTest);
    tcase_add_test(s21_fabs_Case, charEqualTest);

    return s21_fabs_Case;
}
