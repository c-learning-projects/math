#include "tests.h"

START_TEST(normalEqualTest) {
    ssize_t test_arr[] = {4,  16, 144, 10, 30, 31, 32,
                          33, 45, 50,  55, 60, 2,  211};

    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        long double origResult = ceil(test_arr[i]);
        long double s21Result = s21_ceil(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chk norm: ceil(%.2d) = %.8Lf | s21_ceil(%.2d) = %.8Lf\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "fail:  org ceil(%.2lf) = %.8Lf  | s21_ceil(%.2lf) = %.8Lf \n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(lessEqualTest) {
    double test_arr[] = {0,  -10000, -2,    0,    0.5,   -0.5,
                         10, 0.25,   -0.25, 1.99, -1.99, -1000.99};
    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        long double origResult = ceil(test_arr[i]);
        long double s21Result = s21_ceil(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chk less: ceil(%.2lf) = %.8Lf | s21_ceil(%.2lf) = %.8Lf\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "fail less:  org ceil(%.2lf) = %.8Lf | s21_ceil(%.2lf) = %.8Lf\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(moreEqualTest) {
    long double test_arr[] = {111, 1886.9, 33,
                              1024,  // (!) dosnt work with another 1886.9
                              0};    //  (!!) canceled by timeout

    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    long double origResult;
    long double s21Result;
    for (int i = 0; i < arrMax; i++) {
        origResult = ceil(test_arr[i]);
        s21Result = s21_ceil(test_arr[i]);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "fail more:  org ceil(%.2lf) = %.8Lf | s21_ceil(%.2lf) = %.8Lf\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(more1EqualTest) {
    long double test_arr[] = {1111};  //  (!!) canceled by timeout

    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    long double origResult;
    long double s21Result;
    for (int i = 0; i < arrMax; i++) {
        origResult = ceil(test_arr[i]);
        s21Result = s21_ceil(test_arr[i]);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "f more1:  org ceil(%.2lf) = %.8Lf | s21_ceil(%.2lf) = %.8Lf\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(charEqualTest) {
    char test_arr[] = {'3', '7', 't', 'Q', '\n', '\t'};
    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        long double origResult = ceil(test_arr[i]);
        long double s21Result = s21_ceil(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chk chr: ceil(%.2d) = %.8Lf | s21_ceil(%.2d) = %.8Lf\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "chr fail: org ceil(%.2d) = %.8Lf | s21_ceil(%.2d) = %.8Lf\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

TCase* Creates_21_ceil_Case() {
    TCase* s21_ceil_Case = tcase_create("s21_ceil_Case");

    tcase_add_test(s21_ceil_Case, normalEqualTest);
    tcase_add_test(s21_ceil_Case, lessEqualTest);
    tcase_add_test(s21_ceil_Case, moreEqualTest);
    tcase_add_test(s21_ceil_Case, more1EqualTest);
    tcase_add_test(s21_ceil_Case, charEqualTest);

    return s21_ceil_Case;
}
