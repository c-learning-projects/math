#include "tests.h"

START_TEST(normalEqualTest) {
    double test_arr[] = {-4.9, -1.6, 14.4, 1.0, 3.0,  3.1, 3.2,
                         3.3,  -4.5, 5.0,  5.5, -6.0, 2.,  -.211};

    double arrMax = (double)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        double origResult = floor(test_arr[i]);
        double s21Result = s21_floor(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chknrm: orig floor(%.2d) = %.8d | s21_floor(%.2d) = %.8d\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "failnrm: orig floor(%.2d) = %.8d | s21_floor(%.2d) = %.8d\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(lessEqualTest) {
    double test_arr[] = {-.4000,   -1.6000, -1440000, -100000,
                         -300.000, -3.1111, -111.32};

    double arrMax = (double)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        double origResult = floor(test_arr[i]);
        double s21Result = s21_floor(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chkls: orig floor(%.2d) = %.8d | s21_floor(%.2d) = %.8d\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "faills: orig floor(%.2d) = %.8d | s21_floor(%.2d) = %.8d\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(moreEqualTest) {
    double test_arr[] = {+111.1111, -1.6000, -1440.000, +100.6000,
                         +30000.0,  -31.111, -111.32};

    double arrMax = (double)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        double origResult = floor(test_arr[i]);
        double s21Result = s21_floor(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chk mo: orig floor(%.2d) = %.8d | s21_floor(%.2d) = %.8d\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "fail mo: orig floor(%.2d) = %.8d | s21_floor(%.2d) = %.8d\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(charEqualTest) {
    char test_arr[] = {'3', '7', 't', 'Q', '\n', '\t'};
    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        long double origResult = floor(test_arr[i]);
        long double s21Result = s21_floor(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chk chr: sqrt(%.2d) = %.8Lf | s21_sqrt(%.2d) = %.8Lf\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "chr fail: org floor(%.2d) = %.8Lf | s21_floor(%.2d) = %.8Lf\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

TCase* Creates_21_floor_Case() {
    TCase* s21_floor_Case = tcase_create("s21_floor_Case");

    tcase_add_test(s21_floor_Case, normalEqualTest);
    tcase_add_test(s21_floor_Case, lessEqualTest);
    tcase_add_test(s21_floor_Case, moreEqualTest);
    tcase_add_test(s21_floor_Case, charEqualTest);

    return s21_floor_Case;
}
