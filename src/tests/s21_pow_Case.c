#include "tests.h"

START_TEST(normalEqualTest) {
    double test_arr[][2] = {{2, 2},  {2, 0.5}, {2, 3.5}, {0, 2},   {2, 0},
                            {18, 6}, {-4, 2},  {8, -3},  {-8, -3}, {-8, -3.5}};

    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        long double origResult = pow(test_arr[i][0], test_arr[i][1]);
        long double s21Result = s21_pow(test_arr[i][0], test_arr[i][1]);
        // (!)uncomment to watch(!) :
        // printf("chk norm: pow(%.2lf^%.2lf) = %.8Lf | s21_pow(%.2lf^%.2lf) =
        // %.8Lf\n",
        //        test_arr[i][0], test_arr[i][0],
        // test_arr[i][1], origResult, test_arr[i][1],
        //        s21Result);
        ck_assert_msg(d_comp(origResult, s21Result),
                      "\nfail nrm:  org pow(%.2lf^%.2lf) = %.8Lf  | "
                      "s21_pow(%.2lf^%.2lf) = %.8Lf \n",
                      test_arr[i][0], test_arr[i][1], origResult,
                      test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

START_TEST(lessEqualTest) {
    double test_arr[][2] = {{-0.2, 2},  {0.02, -0.5}, {0.2, -3.5}, {-0.0, 2},
                            {0.2, -0},  {-1.8, -16},  {-0.4, 2},   {0.8, -3},
                            {-0.8, -3}, {-0.8, -3.5}};
    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        long double origResult = pow(test_arr[i][0], test_arr[i][1]);
        long double s21Result = s21_pow(test_arr[i][0], test_arr[i][1]);
        // (!)uncomment to watch(!) :
        // printf("chk less: pow(%.2lf^%.2lf) = %.8Lf | s21_pow(%.2lf^%.2lf) =
        // %.8Lf\n",
        //        test_arr[i][0], test_arr[i][0],
        // test_arr[i][1], origResult, test_arr[i][1],
        //        s21Result);
        ck_assert_msg(d_comp(origResult, s21Result),
                      "\nfail less:  org pow(%.2lf^%.2lf) = %.8Lf | "
                      "s21_pow(%.2lf^%.2lf) = %.8Lf\n",
                      test_arr[i][0], test_arr[i][1], origResult,
                      test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

START_TEST(moreEqualTest) {
    double test_arr[][2] = {{20, 2},     {2, 0.5},    {2, 3.5},   {1110, 2},
                            {2, 0},      {185, 16},   {-4, 19},   {8, -31},
                            {-8, -3.88}, {-8, -3.05}, {1111, 0.6}};

    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    double origResult;
    long double s21Result;
    for (int i = 0; i < arrMax; i++) {
        origResult = pow(test_arr[i][0], test_arr[i][1]);
        s21Result = s21_pow(test_arr[i][0], test_arr[i][1]);
        ck_assert_msg(d_comp(origResult, s21Result),
                      "\nfail more:  org pow(%.2f^%.2f) = %.8f | "
                      "s21_pow(%.2f^%.2f) = %.8Lf\n",
                      test_arr[i][0], test_arr[i][1], origResult,
                      test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

START_TEST(charEqualTest) {
    char test_arr[][2] = {{'3', 2}, {'7', 0.5}, {'t', 3.5},
                          {'Q', 2}, {'\n', 0},  {'\t'}};
    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        long double origResult = pow(test_arr[i][0], test_arr[i][1]);
        long double s21Result = s21_pow(test_arr[i][0], test_arr[i][1]);
        // (!)uncomment to watch(!) :
        // printf("chk chr: pow(%.2d^%.2d) = %.8Lf | s21_pow(%.2d^%.2d) =
        // %.8Lf\n",
        //        test_arr[i][0], test_arr[i][0],
        // test_arr[i][1], origResult, test_arr[i][1],
        //        s21Result);
        ck_assert_msg(d_comp(origResult, s21Result),
                      "\nchr fail: org pow(%.2d^%.2d) = %.8Lf | "
                      "s21_pow(%.2d^%.2d) = %.8Lf\n",
                      test_arr[i][0], test_arr[i][1], origResult,
                      test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

TCase* Creates_21_pow_Case() {
    TCase* s21_pow_Case = tcase_create("s21_pow_Case");

    tcase_add_test(s21_pow_Case, normalEqualTest);
    tcase_add_test(s21_pow_Case, lessEqualTest);
    tcase_add_test(s21_pow_Case, moreEqualTest);
    tcase_add_test(s21_pow_Case, charEqualTest);

    return s21_pow_Case;
}
