#include "tests.h"

START_TEST(normalEqualTest) {
    int test_arr[] = {-4, -16, 144, 10, 30,  31, 32,
                      33, -45, 50,  55, -60, 2,  -211};

    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        int origResult = abs(test_arr[i]);
        int s21Result = s21_abs(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("check: orig abs(%.2d) = %.8d | s21_abs(%.2d) = %.8d\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(d_comp(origResult, s21Result),
                      "fail:  orig abs(%.2d) = %.8d  | s21_abs(%.2d) = %.8d\n",
                      test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(lessEqualTest) {
    int test_arr[] = {-4000,   -16000, -1440000, -100000,
                      -300000, -31111, -11132};

    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        int origResult = abs(test_arr[i]);
        int s21Result = s21_abs(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("check: orig abs(%.2d) = %.8d | s21_abs(%.2d) = %.8d\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(d_comp(origResult, s21Result),
                      "fail:  orig abs(%.2d) = %.8d  | s21_abs(%.2d) = %.8d\n",
                      test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(moreEqualTest) {
    int test_arr[] = {+1111111111, -16000, -1440000, +100000,
                      +300000,     -31111, -11132};

    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        int origResult = abs(test_arr[i]);
        int s21Result = s21_abs(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("check: orig abs(%.2d) = %.8d | s21_abs(%.2d) = %.8d\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(d_comp(origResult, s21Result),
                      "fail:  orig abs(%.2d) = %.8d  | s21_abs(%.2d) = %.8d\n",
                      test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

START_TEST(charEqualTest) {
    char test_arr[] = {'3', '7', 't', 'Q', '\n', '\t'};
    int arrMax = (int)(sizeof(test_arr) / sizeof(test_arr[0]));
    for (int i = 0; i < arrMax; i++) {
        long double origResult = abs(test_arr[i]);
        long double s21Result = s21_abs(test_arr[i]);
        // (!)uncomment to watch(!) :
        // printf("chk chr: sqrt(%.2d) = %.8Lf | s21_sqrt(%.2d) = %.8Lf\n",
        //        test_arr[i], origResult, test_arr[i], s21Result);
        ck_assert_msg(
            d_comp(origResult, s21Result),
            "chr fail: org abs(%.2d) = %.8Lf | s21_abs(%.2d) = %.8Lf\n",
            test_arr[i], origResult, test_arr[i], s21Result);
    }
}
END_TEST

TCase* Creates_21_abs_Case() {
    TCase* s21_abs_Case = tcase_create("s21_abs_Case");

    tcase_add_test(s21_abs_Case, normalEqualTest);
    tcase_add_test(s21_abs_Case, lessEqualTest);
    tcase_add_test(s21_abs_Case, moreEqualTest);
    tcase_add_test(s21_abs_Case, charEqualTest);

    return s21_abs_Case;
}
