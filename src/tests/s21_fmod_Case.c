#include "tests.h"

START_TEST(normalEqualTest) {
    double test_arr[][2] = {{2, 2},  {2, 0.5}, {2, 3.5}, {0, 2},   {2, 0},
                            {18, 6}, {-4, 2},  {8, -3},  {-8, -3}, {-8, -3.5}};

    int arrMax = 10;
    for (int i = 0; i < arrMax; i++) {
        long double origResult = fmod(test_arr[i][0], test_arr[i][1]);
        long double s21Result = s21_fmod(test_arr[i][0], test_arr[i][1]);
        // (!)uncomment to watch(!) :
        // printf("chk norm: fmod(%.2lf^%.2lf) = %.8Lf | s21_fmod(%.2lf^%.2lf) =
        // %.8Lf\n",
        //        test_arr[i][0], test_arr[i][0],
        // test_arr[i][1], origResult, test_arr[i][1],
            //        s21Result);
            ck_assert_msg(d_comp(origResult, s21Result),
                          "\nfail nrm:  org fmod(%.2lf^%.2lf) = %.8Lf  | "
                          "s21_fmod(%.2lf^%.2lf) = %.8Lf \n",
                          test_arr[i][0], test_arr[i][1], origResult,
                          test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

START_TEST(lessEqualTest) {
    double test_arr[][2] = {{-2, 2},  {2, -0.5},  {2, -3.5}, {-0, 2},
                            {2, -0},  {-18, -16}, {-4, 2},   {8, -3},
                            {-8, -3}, {-8, -3.5}};
    int arrMax = 10;
    for (int i = 0; i < arrMax; i++) {
        long double origResult = fmod(test_arr[i][0], test_arr[i][1]);
        long double s21Result = s21_fmod(test_arr[i][0], test_arr[i][1]);
        // (!)uncomment to watch(!) :
        // printf("chk less: fmod(%.2lf^%.2lf) = %.8Lf | s21_fmod(%.2lf^%.2lf) =
        // %.8Lf\n",
        //        test_arr[i][0], test_arr[i][0],
        // test_arr[i][1], origResult, test_arr[i][1],
            //        s21Result);
            ck_assert_msg(d_comp(origResult, s21Result),
                          "\nfail less:  org fmod(%.2lf^%.2lf) = %.8Lf | "
                          "s21_fmod(%.2lf^%.2lf) = %.8Lf\n",
                          test_arr[i][0], test_arr[i][1], origResult,
                          test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

START_TEST(moreEqualTest) {
    double test_arr[][2] = {{20, 2},     {2, 0.5},   {2, 3.5}, {1110, 2},
                            {2, 0},      {185, 16},  {-4, 19}, {8, -31},
                            {-8, -3.88}, {-8, -3.05}};

    int arrMax = 10;
    long double origResult;
    long double s21Result;
    for (int i = 0; i < arrMax; i++) {
        origResult = fmod(test_arr[i][0], test_arr[i][1]);
        s21Result = s21_fmod(test_arr[i][0], test_arr[i][1]);
        ck_assert_msg(d_comp(origResult, s21Result),
                      "\nfail more:  org fmod(%.2lf^%.2lf) = %.8Lf | "
                      "s21_fmod(%.2lf^%.2lf) = %.8Lf\n",
                      test_arr[i][0], test_arr[i][1], origResult,
                      test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

START_TEST(charEqualTest) {
    char test_arr[][2] = {{'3', 2}, {'7', 0.5}, {'t', 3.5},
                          {'Q', 2}, {'\n', 0},  {'\t'}};
    int arrMax = 6;
    for (int i = 0; i < arrMax; i++) {
        long double origResult = fmod(test_arr[i][0], test_arr[i][1]);
        long double s21Result = s21_fmod(test_arr[i][0], test_arr[i][1]);
        // (!)uncomment to watch(!) :
        // printf("chk chr: fmod(%.2d^%.2d) = %.8Lf | s21_fmod(%.2d^%.2d) =
        // %.8Lf\n",
        //        test_arr[i][0], test_arr[i][0],
        // test_arr[i][1], origResult, test_arr[i][1],
            //        s21Result);
            ck_assert_msg(d_comp(origResult, s21Result),
                          "\nchr fail: org fmod(%.2d^%.2d) = %.8Lf | "
                          "s21_fmod(%.2d^%.2d) = %.8Lf\n",
                          test_arr[i][0], test_arr[i][1], origResult,
                          test_arr[i][0], test_arr[i][1], s21Result);
    }
}
END_TEST

TCase* Creates_21_fmod_Case() {
    TCase* s21_fmod_Case = tcase_create("s21_fmod_Case");

    tcase_add_test(s21_fmod_Case, normalEqualTest);
    tcase_add_test(s21_fmod_Case, lessEqualTest);
    tcase_add_test(s21_fmod_Case, moreEqualTest);
    tcase_add_test(s21_fmod_Case, charEqualTest);

    return s21_fmod_Case;
}
