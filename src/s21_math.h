#ifndef SRC_S21_MATH_H_
#define SRC_S21_MATH_H_
#include <string.h>
#define S21_ACCURITY (long double)0.0000001
#define S21_NAN 0.0 / 0.0
#define S21_POS_INF 1.0 / 0.0
#define S21_NEG_INF -1.0 / 0.0
#define S_21_M_E 2.71828182845904523536028747135266250
#define S21_M_PI 3.14159265358979323846264338327950288
#define S21_M_PI_2 1.57079632679489661923132169163975144
#define S21_M_PI_4 0.785398163397448309615660845819875721
int s21_abs(int x);
long double s21_acos(double x);
long double s21_asin(double x);
long double s21_atan(double x);
long double s21_ceil(double x);
long double s21_cos(double x);
long double s21_exp(double x);
long double s21_fabs(double x);
long double s21_floor(double x);
long double s21_fmod(double x, double y);
long double s21_log(double x);
long double s21_pow(double base, double exp);
long double s21_sin(double x);
long double s21_sqrt(double x);
long double s21_tan(double x);
long double s21_pow2(double base, double exp);
long unsigned s21_myfact(long unsigned x);
#endif  // SRC_S21_MATH_H_
