#include "s21_math.h"

long double s21_pow2(double base, double exp) {
    long double res = base;
    if (exp != 0.0) {
        for (int i = 0; i < (int)exp - 1; i++) res *= base;
    } else {
        res = 1.0;
    }
    return res;
}

long double s21_pow(double base, double exp) {
    if ((base < 0.0) && (exp != (int)exp)) return S21_NAN;
    if (base == 0.0) return 0;
    long double sign = 1.0;
    if ((base < 0.0) && ((((int)exp % 2) != 0))) sign = -1.0;
    long double a = 0, b = 0;
    if ((exp > 0) && (s21_abs(base) > 0)) {
        a = s21_pow2(s21_abs(base), (int)exp);
        b = s21_exp((exp - (int)exp) * s21_log(s21_fabs(base)));
        return a * b * sign;
    } else {
        return s21_exp(exp * s21_log(s21_fabs(base))) * sign;
    }
}

long double s21_fmod(double x, double y) {
    if (y == 0.0) return S21_NAN;
    return x - ((int)(x / y) * y);
}

int s21_abs(int x) {
    if (x < 0) x = -x;
    return x;
}

long double s21_fabs(double x) {
    long double res = x;
    if (x < 0.0) res = -res;
    return res;
}

long double s21_floor(double x) {
    long double res = 0.0;
    if ((x > 0.0) || ((x < 0.0) && ((s21_fmod(x, 1.0) == 0.0)))) res = (int)x;
    if ((x < 0.0) && (s21_fmod(x, 1.0) != 0.0)) res = (int)x - 1;
    return res;
}

long double s21_sqrt(double x) {
    asm("fsqrt" : "=t"(x) : "0"(x));
    return x;
}

// long double s21_sqrt(double x) {
//     if (x < 0.0) return S21_NAN;
//     if (x == 0.0) return 0.0;
//     return s21_pow(x, 0.5);
// }

long double s21_cos(double x) {
    asm("fcos" : "=t"(x) : "0"(x));
    return x;
}

long double s21_sin(double x) {
    asm("fsin" : "=t"(x) : "0"(x));
    return x;
}

// long double s21_cos(double x) {
//     long double s = 1.0;
//     long double res = 0.0;
//     long double n = 1.0;
//     x = s21_fmod(x, S21_M_PI * 2);
//     while (s21_fabs(s) > S21_ACCURITY * 0.0001) {
//         res += s;
//         s *= -1.0 * x * x / ((2.0 * n - 1.0) * (2.0 * n));
//         n++;
//     }
//     return res;
// }

// long double s21_sin(double x) {
//     x = s21_fmod(x, S21_M_PI * 2);
//     long double s = x;
//     long double res = 0.0;
//     long double n = 1.0;
//     while (s21_fabs(s) > S21_ACCURITY * 0.0001) {
//         res += s;
//         s *= -1.0 * x * x / ((2.0 * n) * (2.0 * n + 1.0));
//         n++;
//     }
//     return res;
// }

long double s21_log(double x) {
    asm("fldln2\n\t"
        "fxch\n\t"
        "fyl2x\n\t"
        : "=t"(x)
        : "0"(x));
    return x;
}

// long double s21_log(double x) {
//     if (x < 0.0) return S21_NAN;
//     if (x == 0.0) return S21_NEG_INF;
//     double res = 0.0;
//     long double s_res = 1.0;
//     long double i = 1.0;
//     if (x >= 0.0 && x < 2.0) {
//         x -= 1.0;
//         while (s21_fabs(s_res) > S21_ACCURITY * 0.001) {
//             s_res = s21_pow2(x, i) / i - s21_pow2(x, i + 1) / (i + 1.0);
//             res += s_res;
//             i = i + 2.0;
//         }
//     } else if (x >= 2.0) {
//         x = ((x - 1) / (x));
//         long double tempx_x = x;
//         res = x;
//         i++;
//         while (s21_fabs(s_res) > S21_ACCURITY * 0.001) {
//             tempx_x *= x;
//             s_res = 1.0 / i * tempx_x;
//             res += s_res;
//             i++;
//         }
//     }
//     return res;
// }

long double s21_exp(double x) {
    asm("fldl2e\n\t"
        "fmulp %%st(1)\n\t"
        "fld1\n\t"
        "fscale\n\t"
        "fxch\n\t"
        "fld1\n\t"
        "fxch\n\t"
        "fprem\n\t"
        "f2xm1\n\t"
        "faddp %%st(1)\n\t"
        "fmulp %%st(1)\n\t"
        : "=t"(x)
        : "0"(x));
    return x;
}

// long double s21_exp(double x) {
//     if (x > 211) return S21_POS_INF;
//     if (x < -32) return 0.0;
//     long double f = x;
//     double d_res = 1.0;
//     long double ld_res = 1.0;
//     long double n = 2.0;
//     if (s21_fabs(x) > 10.0) {
//     while (s21_fabs(f) > S21_ACCURITY) {
//         d_res += f;
//         f *= x / n;
//         n++;
//     } return d_res;
//     } else {
//         while (s21_fabs(f) > S21_ACCURITY) {
//         ld_res += f;
//         f *= x / n;
//         n++;
//     } return ld_res;
//     }
// }

long double s21_ceil(double x) {
    if (x == (int)x) return (int)x;
    if (x < 0.0) return (int)x;
    return (int)x + 1.0;
}

long double s21_tan(double x) { return s21_sin(x) / s21_cos(x); }

long double s21_atan(double x) {
    asm("fld1\n\t"
        "FPATAN"
        : "=t"(x)
        : "0"(x));
    return x;
}

// long double s21_atan(double x) {
//     if (x == 1) return S21_M_PI / 4;
//     long double s;
//     long double res = 0.0;
//     long double sign = -1.0;
//     long double n = 3.0;
//     int flags = 0;
//     if (x < 0.0) {
//         x = -x;
//         flags |= 1;
//     }
//     if (x > 1.0) {
//         x = 1.0 / x;
//         flags |= 2;
//     }
//     s = x;
//     while (n < 50) {
//         res += s;
//         s = sign * s21_pow2(x, n) / n;
//         sign = -sign;
//         n += 2.0;
//     }
//     if (flags & 2) res = S21_M_PI_2 - res;
//     if (flags & 1) res = -res;
//     return (res);
// }

long double s21_asin(double x) { return s21_atan(x / s21_sqrt(1 - x * x)); }

long double s21_acos(double x) { return S21_M_PI_2 - s21_asin(x); }
